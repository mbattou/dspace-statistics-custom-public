<%--
    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/
--%>

<%--
  -    statsVisits - StatisticsBean
  -    statsMonthlyVisits - StatisticsBean
  -    statsCountryVisits - StatisticsBean
  -    statsFileDownloads - StatisticsBean
  -    statsMonthlyDownloads - StatisticsBean
  -    statsCountryDownloads - StatisticsBean
  -    statsItemVisits - StatisticsBean
  -    isItem - boolean
  -    isCollection - boolean
--%>
  
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="org.dspace.app.webui.servlet.MyDSpaceServlet"%>
<%@ page import="org.dspace.app.webui.components.StatisticsBean" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.List" %>
<%@ page import="java.awt.*"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.Map" %>

<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="javax.servlet.jsp.PageContext" %>

<%@ page import="org.apache.commons.lang.StringUtils" %>

<%@ page import="org.dspace.app.webui.servlet.admin.AuthorizeAdminServlet" %>
<%@ page import="org.dspace.app.webui.servlet.admin.EditItemServlet" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.content.authority.MetadataAuthorityManager" %>
<%@ page import="org.dspace.content.authority.ChoiceAuthorityManager" %>
<%@ page import="org.dspace.content.authority.Choices" %>
<%@ page import="org.dspace.content.Bitstream" %>
<%@ page import="org.dspace.content.BitstreamFormat" %>
<%@ page import="org.dspace.content.Bundle" %>
<%@ page import="org.dspace.content.Collection" %>
<%@ page import="org.dspace.content.DCDate" %>
<%@ page import="org.dspace.content.DCValue" %>
<%@ page import="org.dspace.content.Item" %>
<%@ page import="org.dspace.content.ItemIterator" %>
<%@ page import="org.dspace.content.MetadataField" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.Constants" %>
<%@ page import="org.dspace.core.Utils" %>
<%@ page import="org.dspace.eperson.EPerson" %>

<%
Locale sessionLocale = UIUtil.getSessionLocale(request);
String handle = (String)request.getAttribute("handle");
Boolean isItem = (Boolean)request.getAttribute("isItem");
Boolean isCollection = (Boolean)request.getAttribute("isCollection");
%>

<dspace:layout titlekey="jsp.statistics.title">

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<div class="row panel">
	<div class="col-md-12">
			
		<h3>
			<c:forEach items="${statsVisits.matrix}" var="row" varStatus="counter">
			<c:out value="${statsVisits.colLabels[counter.index]}"/>
			</c:forEach>
		</h3>
		
		<h5><fmt:message key="jsp.statistics.title"/></h5>
					
		<div class="panel-content">

			<div class="row">
				<div class="col-md-9">
			
					<!-- Page Views -->
					
					<h4><fmt:message key="jsp.statistics.heading.pageviews"/></h4>
					
					<table class="statsTable table" style="width:640px">
						<c:forEach items="${statsVisits.matrix}" var="row" varStatus="counter">
							<c:forEach items="${row}" var="cell" varStatus="rowcounter">
								<c:choose>
									<c:when test="${rowcounter.index % 2 == 0}">
										<c:set var="rowClass" value="evenRowOddCol"/>
									</c:when>
									<c:otherwise>
										<c:set var="rowClass" value="oddRowOddCol"/>
									</c:otherwise>
								</c:choose>
								<tr class="${rowClass}">
									<td>
										<fmt:message key="jsp.statistics.totalviews"/>
										<!--
										<%
										if (isItem) {
											Integer day = (Integer)request.getAttribute("day");
											Integer month = (Integer)request.getAttribute("month");
											Integer year = (Integer)request.getAttribute("year");
										%>
										<em>
										<fmt:message key="jsp.statistics.since">
											<fmt:param value="<%= Integer.toString(day) %>"/>
											<fmt:param value="<%= DCDate.getMonthName(month, UIUtil.getSessionLocale(request)) %>"/>
											<fmt:param value="<%= Integer.toString(year) %>"/>
										</fmt:message>
										</em>
										<% } %>
										-->
									</td>
									<td>
										<c:out value="${cell}"/>
									</td>
								</tr>
							</c:forEach>
						</c:forEach>
					</table>
					
					<!-- File Downloads (Item only) -->
					
					<% if (isItem) { %>
					
					<h4><fmt:message key="jsp.statistics.heading.filedownloads"/></h4>
			
					<%
					StatisticsBean sfd = (StatisticsBean)request.getAttribute("statsFileDownloads");
					List<String> file_downloads_labels = sfd.getColLabels();
					String[][] file_downloads_data = sfd.getMatrix();
					if (file_downloads_data.length == 0 || file_downloads_labels.size() == 0) { %>
					<fmt:message key="jsp.statistics.empty"/>
					<% } else { %>
					<table class="statsTable table" style="width:640px">
						<%	
						for (int i = 0; i < file_downloads_labels.size(); i++) {
						%>
						<tr>
							<td>
								<%= file_downloads_labels.get(i) %>
							</td>
							<td>
								<%= file_downloads_data[0][i]%>
							</td>
						</tr>
						<% } %>
					</table>
					<% }
					
					} %>
					
					<!-- Recent Activity - Last 12 Months -->
			
					<h4><fmt:message key="jsp.statistics.heading.recent"/></h4>
						
					<%
					StatisticsBean smv = (StatisticsBean)request.getAttribute("statsMonthlyVisits");
					String[][] monthly_visits_data = smv.getMatrix();
					List<String> monthly_visits_labels = smv.getColLabels();
					StatisticsBean smd = (StatisticsBean)request.getAttribute("statsMonthlyDownloads");
					List<String> monthly_downloads_column_labels = smd.getColLabels(); 
					String[][] monthly_downloads_data = smd.getMatrix();
					if (monthly_visits_data.length == 0 || monthly_visits_labels.size() == 0) { 
					%>
					<fmt:message key="jsp.statistics.empty"/>
					<% } else { %>
			
					<script type="text/javascript">
						google.load("visualization", "1", {packages:["corechart"]});
						google.setOnLoadCallback(drawChart);
						function drawChart(){
							var data = google.visualization.arrayToDataTable([
							["Month", "<fmt:message key='jsp.statistics.views'/>" <% if (isItem) { %>, "<fmt:message key='jsp.statistics.downloads'/>" <% } %>],
							<%
							for (int j = 0; j < monthly_visits_data.length; j++) {
								for (int i = 0; i < monthly_visits_labels.size(); i++) { %>
									['<%= monthly_visits_labels.get(i) %>', <%= monthly_visits_data[j][i] %> <% if (isItem) { %>, <%= monthly_downloads_data[j][i] %> <% } %>]
									<% if (i < monthly_visits_labels.size()-1) %> ,
							<% } 
							} %>
							]);
							var options = {
								title: '',
								//curveType: 'function',
								colors: ['#428BCA', '#8F001A'],
								legend: { position: 'top', alignment: 'center' }
							};
							var chart = new google.visualization.LineChart(document.getElementById('monthly_views_chart'));
							chart.draw(data, options);
						}
					</script>
			
					<div id="monthly_views_chart" class="statsGraph"></div>
			
					<% } %>
						
					<!-- Geographical Breakdown -->
					
					<h4><fmt:message key="jsp.statistics.heading.geographical"/></h4>
					
					<div id="country-tabs">
					
						<% if (isItem) { %>
						<ul>
							<li><a href="#country-tabs-views"><fmt:message key="jsp.statistics.views"/></a></li>							
							<li><a href="#country-tabs-downloads"><fmt:message key="jsp.statistics.downloads"/></a></li>		
						</ul>
						<% } %>
					
						<% if (isCollection) { %>
						<h5><fmt:message key="jsp.statistics.views"/></h5>
						<% } %>
										
						<div id="country-tabs-views">
						
							<%
							StatisticsBean scv = (StatisticsBean)request.getAttribute("statsCountryVisits");
							List<String> country_visits_labels = scv.getColLabels();
							String[][] country_visits_data = scv.getMatrix();
							if (country_visits_data.length == 0 || country_visits_labels.size() == 0) { %>
							<fmt:message key="jsp.statistics.empty"/>
							<% } else { %>	                  
							<script type="text/javascript">
								google.load("visualization", "1", {packages:["geochart"]});
								// google.setOnLoadCallback(drawRegionsMap1);
								function drawRegionsMap1() {
									var data = google.visualization.arrayToDataTable([
									["Country", ""],
									<% for (int i = 0; i < country_visits_labels.size(); i++) { %> 
										['<%= country_visits_labels.get(i) %>', <%= country_visits_data[0][i] %>]
										<% if (i < country_visits_labels.size() -1) %> ,
									<% } %>
									]);
									var options = {
										colors: ['#428BCA'],
										backgroundColor: '#FFFFFF',
										datalessRegionColor: '#F2F2F2'
									};
									var chart = new google.visualization.GeoChart(document.getElementById('country_visits_chart'));
									chart.draw(data, options);
								}
							</script>
							<div id="country_visits_chart" class="statsGraph"></div>
										
							<% } %>
		
						</div>
		
						<% if (isItem) { %>
						
						<div id="country-tabs-downloads">
		
							<%
							StatisticsBean scd = (StatisticsBean)request.getAttribute("statsCountryDownloads");
							List<String> country_downloads_labels = scd.getColLabels();
							String[][] country_downloads_data = scd.getMatrix();
							if (country_downloads_data.length == 0 || country_downloads_labels.size() == 0) { %>
							<fmt:message key="jsp.statistics.empty"/>
							<% } else { %>	                  
							<script type="text/javascript">
								google.load("visualization", "1", {packages:["geochart"]});
								// google.setOnLoadCallback(drawRegionsMap2);
								function drawRegionsMap2() {
									var data = google.visualization.arrayToDataTable([
									["Country", ""],
									<% for (int i = 0; i < country_downloads_labels.size(); i++) { %> 
										['<%= country_downloads_labels.get(i) %>', <%= country_downloads_data[0][i] %>]
										<% if (i < country_downloads_labels.size() -1) %> ,
									<% } %>
									]);
									var options = {
										colors: ['#8F001A'],
										backgroundColor: '#FFFFFF',
										datalessRegionColor: '#F2F2F2'
									};
									var chart = new google.visualization.GeoChart(document.getElementById('country_downloads_chart'));
									chart.draw(data, options);
								}
							</script>
							<div id="country_downloads_chart" class="statsGraph"></div>
										
							<% } %>
							
						</div>
						
						<% } %>

					</div>
					
					<!-- Most Viewed Items (Collection only) -->
					
					<% if (!isItem && isCollection) { %>
			
					<h4><fmt:message key="jsp.statistics.heading.mostvieweditems"/></h4>
			
					<%
					StatisticsBean siv = (StatisticsBean)request.getAttribute("statsItemVisits");
					List<String> item_visits_labels = siv.getColLabels();
					String[][] item_visits_data = siv.getMatrix();
					if (item_visits_data.length == 0 || item_visits_labels.size() == 0) { %>
					<fmt:message key="jsp.statistics.empty"/>
					<% } else { %>
					<table class="statsTable table" style="width:640px">
						<c:forEach items="${statsItemVisits.matrix}" var="row" varStatus="counter">
							<c:forEach items="${row}" var="cell" varStatus="rowcounter">
								<c:choose>
									<c:when test="${rowcounter.index % 2 == 0}">
										<c:set var="rowClass" value="evenRowOddCol"/>
									</c:when>
									<c:otherwise>
										<c:set var="rowClass" value="oddRowOddCol"/>
									</c:otherwise>
								</c:choose>
								<tr class="${rowClass}">
									<td>
										<c:out value="${statsItemVisits.colLabels[rowcounter.index]}" escapeXml="false"/>
									</td>
									<td>
										<c:out value="${cell}"/>
									</td>
								</tr>
							</c:forEach>
						</c:forEach>
					</table>
			
					<% } 
					} %>

				</div>
			
				<div class="col-md-3">	
					<%
					String doi = (String)request.getAttribute("doi");
					if (doi != null) {
					%>
					<div class="altmetric-embed" data-doi="<%= doi %>" data-badge-type="donut" data-hide-no-mentions="true" data-link-target="_blank" ></div>	
					<% } %>
				</div>
			
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<!-- Note -->
					<br />
					<p><em><fmt:message key="jsp.statistics.note"/></em></p>
				</div>
			</div>
			
		</div>
		
	</div>
</div>

<div class="container row">
	<%
	String backlink = request.getContextPath() + "/handle/" + handle;
	%>
	<a class="btn btn-default" href="<%= backlink %>">
		<% if (isItem) { %>
		<fmt:message key="jsp.statistics.backlink.item"/>
		<% } if (isCollection) { %>
		<fmt:message key="jsp.statistics.backlink.collection"/>
		<% } %>
	</a>
</div>

<script type="text/javascript">
$(function() {
	drawRegionsMap1();
	<% if (isItem) { %>
	drawRegionsMap2();
	$("#country-tabs").tabs();
	<% } %>
});
</script>

<script type='text/javascript' src='https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js'></script>

</dspace:layout>



