/**
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */
package org.dspace.app.webui.servlet;

import java.io.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.client.solrj.response.FacetField.Count;

import org.dspace.app.webui.components.StatisticsBean;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.app.webui.util.UIUtil;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.*;
import org.dspace.content.Bundle;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.eperson.Group;
import org.dspace.handle.HandleManager;
import org.dspace.statistics.Dataset;
import org.dspace.statistics.content.DatasetDSpaceObjectGenerator;
import org.dspace.statistics.content.DatasetTimeGenerator;
import org.dspace.statistics.content.DatasetTypeGenerator;
import org.dspace.statistics.content.StatisticsDataVisits;
import org.dspace.statistics.content.StatisticsListing;
import org.dspace.statistics.content.StatisticsTable;

/**
 *
 * @author Kim Shepherd
 * @version $Revision: 4386 $
 */
public class DisplayStatisticsServlet extends DSpaceServlet {

    private static Logger log = Logger.getLogger(DisplayStatisticsServlet.class);
	
    protected void doDSGet(Context context, HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException, SQLException, AuthorizeException {

		boolean privatereport = ConfigurationManager.getBooleanProperty("usage-statistics", "authorization.admin");

		boolean admin = Group.isMember(context, 1);

		if (!privatereport || admin) {
			displayStatistics(context, request, response);
		}
		else {
			throw new AuthorizeException();
		}
    }

    protected void displayStatistics(Context context, HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException, SQLException, AuthorizeException {
		
        DSpaceObject dso = null;
		Item item = null;
        String handle = request.getParameter("handle");
		Locale sessionLocale = UIUtil.getSessionLocale(request);

        if("".equals(handle) || handle == null) {
            handle = (String)request.getAttribute("handle");
        }

        if (handle != null) {
			dso = HandleManager.resolveToObject(context, handle);
        }

        if (dso == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            JSPManager.showJSP(request, response, "/error/404.jsp");
            return;
        }

		StatisticsBean statsVisits = new StatisticsBean();
        StatisticsBean statsMonthlyVisits = new StatisticsBean();
		StatisticsBean statsCountryVisits = new StatisticsBean();
        StatisticsBean statsFileDownloads = new StatisticsBean();
		StatisticsBean statsMonthlyDownloads = new StatisticsBean();
		StatisticsBean statsCountryDownloads = new StatisticsBean();
		StatisticsBean statsItemVisits = new StatisticsBean();
		
		boolean isItem = (dso.getType() == Constants.ITEM);
		if (isItem) item = (Item) dso; 
		boolean isCollection = (dso.getType() == Constants.COLLECTION) || (dso.getType() == Constants.COMMUNITY);
		
		// default values for stats availability ("since"), corresponds to the date uO Research implemented Solr stats
		int sinceDay = 1;
		int sinceMonth = 1;
		int sinceYear = 2013;
		DCDate statsAvailableFrom = new DCDate (sinceYear, sinceMonth, sinceDay, 0, 0, 0); 
		
		// for items, "since" corresponds instead to dc.date.available
		if (isItem) {
			DCValue[] dateAvailable = item.getMetadata("dc", "date", "available", Item.ANY);			
			if (dateAvailable.length != 0) {
				DCDate itemAvailableFrom = new DCDate(dateAvailable[0].value);				
				if (itemAvailableFrom.toDate().after(statsAvailableFrom.toDate()) == true) {
					if (itemAvailableFrom.getDay() > 0) {
						sinceDay = itemAvailableFrom.getDay();
					}				
					if (itemAvailableFrom.getMonth() > 0) {
						sinceMonth = itemAvailableFrom.getMonth();
					}
					if (itemAvailableFrom.getYear() > 0) {
						sinceYear = itemAvailableFrom.getYear();
					}
				}
			}
		}
		request.setAttribute("day", sinceDay);
		request.setAttribute("month", sinceMonth);
		request.setAttribute("year", sinceYear);
		
		if (isItem) {
			DCValue[] identifierDOI = item.getMetadata("dc", "identifier", "doi", Item.ANY);
			if (identifierDOI.length != 0) {
				request.setAttribute("doi", identifierDOI[0].value); // assume single DOI
			}
		}
		
		/* Page Views */
		
        try {
            
			StatisticsListing statListing = new StatisticsListing(new StatisticsDataVisits(dso));
            statListing.setTitle("Page Views");
            statListing.setId("list1");

            DatasetDSpaceObjectGenerator dsoAxis = new DatasetDSpaceObjectGenerator();
            dsoAxis.addDsoChild(dso.getType(), -1, false, -1);
            statListing.addDatasetGenerator(dsoAxis);
            Dataset dataset = statListing.getDataset(context);

            dataset = statListing.getDataset();

            if (dataset == null) {
				dataset = statListing.getDataset(context);
            }

            if (dataset != null) {
                String[][] matrix = dataset.getMatrix();
                List<String> colLabels = dataset.getColLabels();
                List<String> rowLabels = dataset.getRowLabels();

                statsVisits.setMatrix(matrix);
                statsVisits.setColLabels(colLabels);
                statsVisits.setRowLabels(rowLabels);
            }


		} catch (Exception e) {
			log.error("Error occured while creating statistics for dso with ID: "
						+ dso.getID() + " and type " + dso.getType()
						+ " and handle: " + dso.getHandle(), e);
		}
        
		/* Views - Last 12 Months */

		try {
	
			StatisticsTable statisticsTable = new StatisticsTable(new StatisticsDataVisits(dso));
			
			statisticsTable.setTitle("Views - Last 12 Months");
			statisticsTable.setId("tab1");
			
			DatasetTimeGenerator timeAxis = new DatasetTimeGenerator();
			timeAxis.setDateInterval("month", "-11", "+1");
			statisticsTable.addDatasetGenerator(timeAxis);
			
			DatasetDSpaceObjectGenerator dsoAxis = new DatasetDSpaceObjectGenerator();
			dsoAxis.addDsoChild(dso.getType(), -1, false, -1);
			statisticsTable.addDatasetGenerator(dsoAxis);
			Dataset dataset = statisticsTable.getDataset(context);
			
			dataset = statisticsTable.getDataset();
			
			if (dataset == null) {
				dataset = statisticsTable.getDataset(context);
			}
			
			if (dataset != null) {
			
				String[][] matrix = dataset.getMatrix();
				List<String> colLabels = dataset.getColLabels();
				List<String> rowLabels = dataset.getRowLabels();
			
				statsMonthlyVisits.setMatrix(matrix);
				statsMonthlyVisits.setColLabels(colLabels);
				statsMonthlyVisits.setRowLabels(rowLabels);
			
				// if necessary, translate month labels into french
				if (sessionLocale.toString().equals("fr")) {
					List<String> colLabels_fr = new ArrayList<String>();
					String column_label = null;
					for (int column_counter = 0; column_counter < colLabels.size(); column_counter++) {
						column_label = colLabels.get(column_counter);
						// "September 2015"
						String month = column_label.substring(0, column_label.length()-5); // month == "September"
						String year = column_label.substring(column_label.length()-4); // year == "2015"
						String month_fr = FRENCH_MONTHS.get(month);						
						colLabels_fr.add(month_fr + " " + year);
					} 
					statsMonthlyVisits.setColLabels(colLabels_fr);				
				}
				
			}
			
		} catch (Exception e) {
			log.error("Error occured while creating statistics for dso with ID: "
						+ dso.getID() + " and type " + dso.getType()
						+ " and handle: " + dso.getHandle(), e);
		}

		/* Views - By Country (Item, Collection) */
	
		try {
		
			StatisticsListing statisticsTable = new StatisticsListing(new StatisticsDataVisits(dso));
		
			statisticsTable.setTitle("Views - by Country");
			statisticsTable.setId("tab2");
		
			DatasetTypeGenerator typeAxis = new DatasetTypeGenerator();
			typeAxis.setType("countryCode");
			typeAxis.setMax(10);
			statisticsTable.addDatasetGenerator(typeAxis);
		
			Dataset dataset = statisticsTable.getDataset(context);
		
			dataset = statisticsTable.getDataset();
		
			if (dataset == null) {
				dataset = statisticsTable.getDataset(context);
			}
		
			if (dataset != null) {
				String[][] matrix = dataset.getMatrix();
				List<String> colLabels = dataset.getColLabels();
				List<String> rowLabels = dataset.getRowLabels();
		
				statsCountryVisits.setMatrix(matrix);
				statsCountryVisits.setColLabels(colLabels);
				statsCountryVisits.setRowLabels(rowLabels);
			}
				
		} catch (Exception e) {
			log.error("Error occured while creating statistics for dso with ID: "
						+ dso.getID() + " and type " + dso.getType()
						+ " and handle: " + dso.getHandle(), e);
		}
		
		/* File Downloads (Item only) */
		
		if (isItem) {
			
			try {
		
				StatisticsListing statisticsTable = new StatisticsListing(new StatisticsDataVisits(dso));
			
				statisticsTable.setTitle("File Downloads");
				statisticsTable.setId("tab3");
			
				DatasetDSpaceObjectGenerator dsoAxis = new DatasetDSpaceObjectGenerator();
				dsoAxis.addDsoChild(Constants.BITSTREAM, -1, false, -1);
				statisticsTable.addDatasetGenerator(dsoAxis);
			
				Dataset dataset = statisticsTable.getDataset(context);
			
				dataset = statisticsTable.getDataset();
			
				if (dataset == null) {
					dataset = statisticsTable.getDataset(context);
				}
			
				if (dataset != null) {
					
					String[][] matrix = dataset.getMatrix();
					List<String> colLabels = dataset.getColLabels();
					List<String> rowLabels = dataset.getRowLabels();
					
					statsFileDownloads.setMatrix(matrix);
					statsFileDownloads.setColLabels(colLabels);
					statsFileDownloads.setRowLabels(rowLabels);
					
					// construct list of bitstream hyperlinks for this item
					List<String> colLabels_new = new ArrayList<String>();
					String link = "";
					Bundle[] bundles = item.getBundles("ORIGINAL");
					for (int i = 0; i < bundles.length; i++) {
						Bitstream[] bitstreams = bundles[i].getBitstreams();
						for (int j = 0; j < bitstreams.length; j++) {
							for (int k = 0; k < bitstreams.length; k++) {
								if (bitstreams[k].getFormat().isInternal() == false) {
									if (bitstreams[k].getName().equals(colLabels.get(j))) {
										link = "<a target=\"_blank\" href=\"" + request.getContextPath();
										if ((handle != null) && (bitstreams[k].getSequenceID() > 0)){
											link += "/bitstream/" + handle + "/" + bitstreams[k].getSequenceID() + "/";
										}
										else {
											link += "/retrieve/" + bitstreams[k].getID() + "/";
										}
										link += UIUtil.encodeBitstreamName(bitstreams[k].getName(), Constants.DEFAULT_ENCODING) + "\">";
										link += bitstreams[k].getName();
										link += "</a>"; 
										colLabels_new.add(link);
										break;										
									}
								}
							}
						}
					}

					statsFileDownloads.setColLabels(colLabels_new);
	
				}
				
			} catch (Exception e) {
				log.error("Error occured while creating statistics for dso with ID: "
							+ dso.getID() + " and type " + dso.getType()
							+ " and handle: " + dso.getHandle(), e);
			}
		
		}
		
		/* Downloads - Last 12 Months (Item only) */ 
		
		if (isItem) {
						
			try {
	
				StatisticsListing statisticsTable = new StatisticsListing(new StatisticsDataVisits(dso));
				
				statisticsTable.setTitle("Monthly Downloads");
				statisticsTable.setId("tab4");
				
				DatasetTimeGenerator timeAxis = new DatasetTimeGenerator();
				timeAxis.setDateInterval("month", "-11", "+1");
				statisticsTable.addDatasetGenerator(timeAxis);
				
				DatasetDSpaceObjectGenerator dsoAxis = new DatasetDSpaceObjectGenerator();
				dsoAxis.addDsoChild(Constants.BITSTREAM, -1, false, -1);
				statisticsTable.addDatasetGenerator(dsoAxis);
				
				Dataset dataset = statisticsTable.getDataset(context);
				dataset = statisticsTable.getDataset();
				
				if (dataset == null) {
					dataset = statisticsTable.getDataset(context);
				}
				
				if (dataset != null) {
					
					String[][] matrix = dataset.getMatrix();
					List<String> colLabels = dataset.getColLabels();
					List<String> rowLabels = dataset.getRowLabels();
				
					statsMonthlyDownloads.setMatrix(matrix);
					statsMonthlyDownloads.setColLabels(colLabels);
					statsMonthlyDownloads.setRowLabels(rowLabels);
					
					// if necessary, translate month labels into french
					if (sessionLocale.toString().equals("fr")) {
						List<String> colLabels_fr = new ArrayList<String>();
						String column_label = null;
						for (int column_counter = 0; column_counter < colLabels.size(); column_counter++) {
							column_label = colLabels.get(column_counter);
							// "September 2015"
							String month = column_label.substring(0, column_label.length()-5); // month == "September"
							String year = column_label.substring(column_label.length()-4); // year == "2015"
							String month_fr = FRENCH_MONTHS.get(month);						
							colLabels_fr.add(month_fr + " " + year);
						} 
						statsMonthlyDownloads.setColLabels(colLabels_fr);				
					}
					
				}
					
			} catch (Exception e) {
				log.error("Error occured while creating statistics for dso with ID: "
							+ dso.getID() + " and type " + dso.getType()
							+ " and handle: " + dso.getHandle(), e);
			}
		}
		
		/* Downloads - By Country (Item only) */

		if (isItem) {
			
			try {
		
				StatisticsListing statisticsTable = new StatisticsListing(new StatisticsDataVisits(dso));
				
				statisticsTable.setTitle("Downloads - By Country");
				statisticsTable.setId("tab5");
				
				DatasetTypeGenerator typeAxis = new DatasetTypeGenerator();
				typeAxis.setType("countryCode");
				typeAxis.setMax(10);
				statisticsTable.addDatasetGenerator(typeAxis);
				
				DatasetDSpaceObjectGenerator dsoAxis = new DatasetDSpaceObjectGenerator();
				dsoAxis.addDsoChild(Constants.BITSTREAM, -1, false, -1);
				statisticsTable.addDatasetGenerator(dsoAxis);             
				
				Dataset dataset = statisticsTable.getDataset(context);
				
				dataset = statisticsTable.getDataset();
				
				if (dataset == null) {
					dataset = statisticsTable.getDataset(context);
				}
				
				if (dataset != null) {
					
					String[][] matrix = dataset.getMatrix();
					List<String> colLabels = dataset.getColLabels();
					List<String> rowLabels = dataset.getRowLabels();
				
					statsCountryDownloads.setMatrix(matrix);
					statsCountryDownloads.setColLabels(colLabels);
					statsCountryDownloads.setRowLabels(rowLabels);
				
				}
						
			} catch (Exception e) {
				log.error("Error occured while creating statistics for dso with ID: "
							+ dso.getID() + " and type " + dso.getType()
							+ " and handle: " + dso.getHandle(), e);
			}
		
		} 
		
		/* Most Viewed Items (Collection only) */
		
		if (isCollection) {
			
			HttpSolrServer server = null;
			QueryResponse solrResponse = null;
			final int MAX_MOST_VIEWED = 10;
			
			if (ConfigurationManager.getProperty("solr-statistics", "server") != null) { 
			
				try {
					
					String owningType = "";
					String owningID = "";
					String queryString = ""; 
	
					if (dso instanceof Collection) {
						owningType = "owningColl";
						Collection collection = (Collection) dso;
						owningID = String.valueOf(collection.getID());
					}
					else if (dso instanceof Community) {
						owningType = "owningComm";
						Community community = (Community) dso;
						owningID = String.valueOf(community.getID());
					}
					queryString = "type:2 AND " + owningType + ":" + owningID + " AND isBot:false"; 
			
					server = new HttpSolrServer(ConfigurationManager.getProperty("solr-statistics", "server"));
					SolrQuery query = new SolrQuery(queryString);
					query.setFacet(true);
					query.addFacetField("id");
					query.setFacetMinCount(1);
					query.setFacetLimit(MAX_MOST_VIEWED);
			
					solrResponse = server.query(query);
					
					if (solrResponse != null) {
						
						String[][] matrix = new String[1][MAX_MOST_VIEWED];
						List<String> colLabels = new ArrayList<String>();
						List<String> rowLabels = new ArrayList<String>();
						
						for (int i = 0; i < solrResponse.getFacetFields().size(); i++) {
							FacetField ff = solrResponse.getFacetFields().get(i);
							List<Count> mostViewedItems = ff.getValues();
							for (int j = 0; j < mostViewedItems.size(); j++) {
								Count count = mostViewedItems.get(j);
								int itemID = Integer.valueOf(count.getName());
								String itemViews = String.valueOf(count.getCount());
								DSpaceObject itemDSO = DSpaceObject.find(context, Constants.ITEM, itemID);
								if (itemDSO != null){	
									String itemHandle = itemDSO.getHandle();
									String itemTitle = "";
									if (itemHandle != null) {								
										itemTitle = "<a href=\"" + request.getContextPath() + "/handle/" + itemHandle + "\">";
										itemTitle += itemDSO.getName();
										itemTitle += "</a>";									
									}
									else {
										itemTitle = itemDSO.getName();
									}
									matrix[0][j] = itemViews;
									colLabels.add(itemTitle);
									rowLabels.add("");
								}
							}
						}
						
						statsItemVisits.setMatrix(matrix);
						statsItemVisits.setColLabels(colLabels);
						statsItemVisits.setRowLabels(rowLabels);
						
					}
	
	
				} catch (SolrServerException e) {
					log.error("Error occured while querying Solr", e);
				}
			
			}
		}

		request.setAttribute("statsVisits", statsVisits); // Page Views
		request.setAttribute("statsMonthlyVisits", statsMonthlyVisits); // Views - Last 12 Months
		request.setAttribute("statsCountryVisits", statsCountryVisits); // Views - By Country
		request.setAttribute("statsFileDownloads", statsFileDownloads); // File Downloads (Item only)
		request.setAttribute("statsMonthlyDownloads", statsMonthlyDownloads); // Downloads - Last 12 Months (Item only)
		request.setAttribute("statsCountryDownloads", statsCountryDownloads); // Downloads - By Country (Item only)
		request.setAttribute("statsItemVisits", statsItemVisits); // Most Viewed Items (Collection only)		
		request.setAttribute("isItem", isItem);
		request.setAttribute("isCollection", isCollection);
	
		JSPManager.showJSP(request, response, "display-statistics.jsp");
        
	}
	
	private static final Map<String, String> FRENCH_MONTHS = createFrenchMonthMap();
	
    private static Map<String, String> createFrenchMonthMap() {
        Map<String, String> result = new HashMap<String, String>();
		result.put("January", "Janvier");
		result.put("February", "Février"); 
		result.put("March", "Mars"); 
		result.put("April", "Avril"); 
		result.put("May", "Mai"); 
		result.put("June", "Juin");
		result.put("July", "Juillet"); 
		result.put("August", "Août");
		result.put("September", "Septembre");
		result.put("October", "Octobre"); 
		result.put("November", "Novembre");
		result.put("December", "Décembre"); 
        return Collections.unmodifiableMap(result);
    }
	
}